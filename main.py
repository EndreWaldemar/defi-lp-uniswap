import web3
from eth_abi.packed import encode_abi_packed
from uniswap import Uniswap
from web3 import Web3
import os

address = None  # or None if you're not going to make transactions
private_key = None  # or None if you're not going to make transactions
version = 2  # specify which version of Uniswap to use
provider = os.getenv("PROVIDER")
uniswap = Uniswap(address=address, private_key=private_key, version=version, provider=provider)

weth = "0x7ceb23fd6bc0add59e62ac25578270cff1b9f619"
weth = Web3.toChecksumAddress(weth)
weth_decimals = 18
usdc = "0x2791bca1f2de4661ed88a30c99a7a9449aa84174"
usdc = Web3.toChecksumAddress(usdc)
usdc_decimals = 6


def get_pair(coin1, coin2):
    """
    gets the LP contract address given 2 token addresses
    :param coin1:
    :param coin2:
    :return: LP contract address
    """
    quickswap_factory = "0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32"
    # hexadem is some magical etherum uniswap code
    hexadem_ = '0x96e8ac4277198ff8b6f785478aa9a39f403cb768dd02cbee326c3e7da348845f'
    abiEncoded_1 = encode_abi_packed(['address', 'address'], (coin1, coin2))
    salt_ = Web3.solidityKeccak(['bytes'], ['0x' + abiEncoded_1.hex()])
    abiEncoded_2 = encode_abi_packed(['address', 'bytes32'], (quickswap_factory, salt_))
    lp_pair = Web3.solidityKeccak(['bytes', 'bytes'], ['0xff' + abiEncoded_2.hex(), hexadem_])[12:]
    return lp_pair


def get_token_price_in_eth(owner, weth_address, token):
    """
    this function does not work
    :param owner: owner address of the LP token
    :param weth_address: address for the etherum address
    :param token: token
    :return:
    """
    # weth = Web3.toChecksumAddress(weth_address)
    erc20_weth = uniswap.w3.eth.contract(address=weth)
    owner = Web3.toChecksumAddress(owner)  # Uni:Token input exchange ex: UniV2:DAI
    weth_balance = erc20_weth.functions.balanceOf(owner).call()
    weth_balance = float(web3.fromWei(weth_balance, 'ether'))
    print(f'WETH quantity in Uniswap Pool = {weth_balance}')
    token_address = Web3.toChecksumAddress(token)  # Like DAI
    erc20 = uniswap.w3.eth.contract(address=token_address)
    token_balance: int = erc20.functions.balanceOf(owner).call()
    gwei = 1000000000
    token_balance = float(token_balance / gwei)
    print(f'Token balance in Uniswap Pool = {token_balance}')
    return float(weth_balance / token_balance)  # price of token


def get_eth_price():
    """
    :return: etherum USD price
    """
    price = uniswap.get_price_input(weth, usdc, 10 ** 18)
    usdc_price_float = float(price / (10 ** usdc_decimals))
    return usdc_price_float


if __name__ == "__main__":
    owner_of_usdc_weth = get_pair(usdc, weth)
    lp_price = get_token_price_in_eth(owner_of_usdc_weth, weth, usdc)

    print(f"price of the USDC/WETH QS LP: {lp_price}")
